import pygame
from pygame.sprite import Sprite

class Bullet(Sprite):
    def __init__(self, ai_game):
        '''A class to manage bullets fired form the ship'''
        super().__init__()
        self.screen = ai_game.screen
        self.settings = ai_game.setting
        self.colour = self.settings.bullet_colour

        # Create a bullet rect at (0, 0) and then set correct postions
        self.rect = pygame.Rect(0,0, self.settings.bullet_width,
            self.settings.bullet_height)
        self.rect.midtop = ai_game.ship.rect.midtop

        # Store the bullet's postion as decimal value.
        self.y = float(self.rect.y)

    def update(self):
        '''Update the bullet up the screen.'''
        self.y -= self.settings.bullet_speed
        # Update the rect postion.
        self.rect.y = self.y
        
    def draw_bullet(self):
        '''Draw the bullet to the screen.'''
        pygame.draw.rect(self.screen, self.colour, self.rect)


