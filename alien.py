import pygame
from pygame.sprite import Sprite

class Alien(Sprite):
    '''A class the represent a single alien in the fleet'''
    def __init__(self, ai_game):
        super().__init__()
        self.screen = ai_game.screen
        self.settings = ai_game.setting

        #Alien Settings
        self.health = 1

        #Load the alien image and set its rect attribute
        self.image = pygame.image.load('images/alien.bmp')
        self.rect = self.image.get_rect()

        #Start each new alien near the top left of the screen.
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height

        # Store the aliens near the top left of the screen
        self.x = float(self.rect.x)

    def update(self):
        '''Move the alien right or left.'''
        self.x += (self.settings.alien_speed *
                        self.settings.fleet_direction)
        self.rect.x = self.x

    def check_edges(self):
        '''Return true if alien is at the edge of the scren'''
        screen_rect = self.screen.get_rect()
        if self.rect.right >= screen_rect.right or self.rect.left <= 0:
            return True

class Alien_L2(Alien):
    '''A class the represent a stronger alien in the fleet'''
    def __init__(self, ai_game):
        super().__init__(ai_game)
        self.health = 2
        self.image = pygame.image.load('images/red_alien.bmp')