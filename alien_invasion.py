import sys
from time import sleep
import pygame

from setting import Settings
from ship import Ship
from bullet import Bullet
from alien import Alien, Alien_L2
from game_stat import GameStats
from scoreboard import Scoreboard
from button import Button

class AlienInvasion:
    '''Overall class to manage game assets and behavior'''

    def __init__(self):
        '''Initialize the game, and create game resources'''
        pygame.init()
        self.setting = Settings()
        #Setting Screen Dimensions
        self.screen = pygame.display.set_mode((self.setting.screen_width,
            self.setting.screen_height))
        pygame.display.set_caption("Alien Invasion")
        # Create an instance to store game statistics, and create a scoreboard
        self.stats = GameStats(self)
        self.sb = Scoreboard(self)
        # Set background colour.
        self.bg_colour = self.setting.bg_colour
        self.ship = Ship(self)
        self.bullets = pygame.sprite.Group()
        self.aliens = pygame.sprite.Group()
        self._create_alien_fleet()
        # Start Alien Invasion in an active state.
        self.game_active = True
        # Make the Play button.
        self.play_button = Button(self, "Play")

    def run_game(self):
        '''Start the main loop for game'''
        while True:
            self._check_events_()
            if self.stats.game_active:
                self.ship.update()
                self._update_bullets()
                self._update_aliens()
            self._update_screen()

    def _update_screen(self):
        # Redraw the screen during each pass through the loop
        self.screen.fill(self.bg_colour)
        self.ship.blitme()
        # Make the most resently drawn screen visable.
        for bullet in self.bullets.sprites():
            bullet.draw_bullet()
        self.aliens.draw(self.screen)

        # Draw the score information.
        self.sb.show_score()

        # Draw the play button if the game is inactive
        if not self.stats.game_active:
            self.play_button.draw_button()
        pygame.display.flip()

    def _check_events_(self):
        # Watch for keyboard and mouse events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                self._check_keydown_events(event)
            elif event.type == pygame.KEYUP:
                self._check_keyup_events(event)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                self._check_play_button(mouse_pos)

    def _check_keydown_events(self, event):
        '''responds to keydown events'''
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = True
        if event.key == pygame.K_LEFT:
            self.ship.moving_left = True
        elif event.key == pygame.K_q:
            sys.exit()
        elif event.key == pygame.K_SPACE and not self.stats.game_active:
            self.start_game()
        elif event.key == pygame.K_SPACE:
            self._fire_bullet()

    def _check_keyup_events(self, event):
        '''responds to keyup events'''
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = False
        if event.key == pygame.K_LEFT:
            self.ship.moving_left = False

    def _fire_bullet(self):
        '''Create a new bullet and add it to the bullet group'''
        if len(self.bullets) < self.setting.bullets_allowed:
            new_bullet = Bullet(self)
            self.bullets.add(new_bullet)

    def _update_bullets(self):
        '''Update postion of bullets and get rid of old bullets'''
        self.bullets.update()
        # Get rid of  bullets that have disappeared.
        for bullet in self.bullets.copy():
            if bullet.rect.bottom <= 0:
                self.bullets.remove(bullet)

        self._check_bullet_alien_collision()

    def _check_bullet_alien_collision(self):
        collisions = pygame.sprite.groupcollide(
                 self.bullets, self.aliens, True, False)

        if collisions and self._check_alien_health(collisions):
            for aliens in collisions.values():
                self.stats.score += self.setting.alien_points * len(aliens)
            self.sb.prep_score()
            self.sb.check_high_score()

        if not self.aliens:
        # Destroy Existing Bullets and create new fleet
            self.bullets.empty()
            self._create_alien_fleet()
            self.setting.increase_speed()

            # Increase level.
            self.stats.level += 1
            self.sb.prep_level()

    def _check_alien_health(self, collisions):
        for collision in collisions.values():
            for sprite in collision:
                sprite.health -= 1
            if sprite.health == 0:
                sprite.kill()
                return True

    def _create_alien_fleet(self):
        '''Create the fleet of aliens'''
        # Make an Alien and find the number of aliens in a row
        # Spacing between each alien is equal to one alien width
        alien = Alien_L2(self)
        self.aliens.add(alien)

        alien_width, alien_height = alien.rect.size
        available_space_x = self.setting.screen_width - (2 * alien_width)
        number_aliens_x = available_space_x // (2 * alien_width)

        # Determine the number of rows of aliens that fit on the screen
        ship_height = self.ship.rect.height
        available_space_y = (self.setting.screen_height -
                                (3 * alien_height) - ship_height)
        number_rows = available_space_y // (2 * alien_height)

        # Create the firest row of aliens.
        for row_number in range(number_rows):
            for alien_number in range(number_aliens_x):
                self.create_alien(alien_number, row_number)

    def create_alien(self, alien_number, row_number):
        '''Create an Alien and place it in the row'''
        alien = Alien_L2(self)
        alien_width, alien_height = alien.rect.size
        alien_width = alien.rect.width
        alien.x = alien_width + 2 * alien_width * alien_number
        alien.rect.x = alien.x
        alien.rect.y = alien.rect.height + 2 * alien.rect.height * row_number
        self.aliens.add(alien)

    def _check_fleet_edges(self):
        '''Respond appropriately if any aliens have reached the edge'''
        for alien in self.aliens.sprites():
            if alien.check_edges():
                self._change_fleet_direction()
                break

    def _change_fleet_direction(self):
        '''Drop the entire fleet and change the fleet's direction'''
        for alien in self.aliens.sprites():
            alien.rect.y += self.setting.fleet_drop_speed
        self.setting.fleet_direction *= -1

    def _update_aliens(self):
        '''Update the postions of all aliens in the fleet'''
        self._check_fleet_edges()
        self.aliens.update()

        # Look for alien-ship collisions.
        if pygame.sprite.spritecollideany(self.ship, self.aliens):
            self._ship_hit()

        # Look for aliens hititng the bottom of the screen.
        self._check_aliens_bottom()

    def _ship_hit(self):
        '''respond to the ship being hit by an alien.'''
        if self.stats.ships_left > 0:
            # Decrement ships_left, and update scoreboard.
            self.stats.ships_left -= 1
            self.sb.prep_ships()

            # Get rid of any remaining aliens and bullets
            self.aliens.empty()
            self.bullets.empty()

            # Create a new fleet and center th ship
            self._create_alien_fleet()
            self.ship.center_ship()

            # Pause.
            sleep(0.5)
        else:
            self.stats.game_active = False
            # show the mouse cursor.
            pygame.mouse.set_visible(True)

    def _check_aliens_bottom(self):
        '''Check if any aliens have reached the bottom of the screen'''
        screen_rect = self.screen.get_rect()
        for alien in self.aliens.sprites():
            if alien.rect.bottom >= screen_rect.bottom:
                # Treat this the same as if  the ship got hit.
                self._ship_hit()
                break

    def _check_play_button(self, mouse_pos):
        '''Checks for play button click.'''
        button_clicked = self.play_button.rect.collidepoint(mouse_pos)
        if button_clicked and not self.stats.game_active:
            self.sb.prep_score()
            self.sb.prep_level()
            self.sb.prep_ships()
            self.start_game()

    def start_game(self):
        '''Start a new game.'''
        #Reset Statistics.
        self.stats.reset_stats()
        self.stats.game_active = True

        # Get rid of any remaining aliens and bullets.
        self.aliens.empty()
        self.bullets.empty()

        # Create a new fleet and center te ship
        self._create_alien_fleet()
        self.ship.center_ship()

        # Hide the mouse cursor.
        pygame.mouse.set_visible(False)

if __name__ == '__main__':
    # Make a game instance, and run the game
    ai = AlienInvasion()
    ai.run_game()
